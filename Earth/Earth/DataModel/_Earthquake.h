// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Earthquake.h instead.

#import <CoreData/CoreData.h>

extern const struct EarthquakeAttributes {
	__unsafe_unretained NSString *depth;
	__unsafe_unretained NSString *eqId;
	__unsafe_unretained NSString *latitude;
	__unsafe_unretained NSString *longitude;
	__unsafe_unretained NSString *magnitude;
	__unsafe_unretained NSString *region;
	__unsafe_unretained NSString *timeDate;
} EarthquakeAttributes;

@interface EarthquakeID : NSManagedObjectID {}
@end

@interface _Earthquake : NSManagedObject {}
+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_;
+ (NSString*)entityName;
+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_;
@property (nonatomic, readonly, strong) EarthquakeID* objectID;

@property (nonatomic, strong) NSNumber* depth;

@property (atomic) float depthValue;
- (float)depthValue;
- (void)setDepthValue:(float)value_;

//- (BOOL)validateDepth:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* eqId;

//- (BOOL)validateEqId:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* latitude;

@property (atomic) double latitudeValue;
- (double)latitudeValue;
- (void)setLatitudeValue:(double)value_;

//- (BOOL)validateLatitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* longitude;

@property (atomic) double longitudeValue;
- (double)longitudeValue;
- (void)setLongitudeValue:(double)value_;

//- (BOOL)validateLongitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSNumber* magnitude;

@property (atomic) float magnitudeValue;
- (float)magnitudeValue;
- (void)setMagnitudeValue:(float)value_;

//- (BOOL)validateMagnitude:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSString* region;

//- (BOOL)validateRegion:(id*)value_ error:(NSError**)error_;

@property (nonatomic, strong) NSDate* timeDate;

//- (BOOL)validateTimeDate:(id*)value_ error:(NSError**)error_;

@end

@interface _Earthquake (CoreDataGeneratedPrimitiveAccessors)

- (NSNumber*)primitiveDepth;
- (void)setPrimitiveDepth:(NSNumber*)value;

- (float)primitiveDepthValue;
- (void)setPrimitiveDepthValue:(float)value_;

- (NSString*)primitiveEqId;
- (void)setPrimitiveEqId:(NSString*)value;

- (NSNumber*)primitiveLatitude;
- (void)setPrimitiveLatitude:(NSNumber*)value;

- (double)primitiveLatitudeValue;
- (void)setPrimitiveLatitudeValue:(double)value_;

- (NSNumber*)primitiveLongitude;
- (void)setPrimitiveLongitude:(NSNumber*)value;

- (double)primitiveLongitudeValue;
- (void)setPrimitiveLongitudeValue:(double)value_;

- (NSNumber*)primitiveMagnitude;
- (void)setPrimitiveMagnitude:(NSNumber*)value;

- (float)primitiveMagnitudeValue;
- (void)setPrimitiveMagnitudeValue:(float)value_;

- (NSString*)primitiveRegion;
- (void)setPrimitiveRegion:(NSString*)value;

- (NSDate*)primitiveTimeDate;
- (void)setPrimitiveTimeDate:(NSDate*)value;

@end
