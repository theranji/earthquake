// DO NOT EDIT. This file is machine-generated and constantly overwritten.
// Make changes to Earthquake.m instead.

#import "_Earthquake.h"

const struct EarthquakeAttributes EarthquakeAttributes = {
	.depth = @"depth",
	.eqId = @"eqId",
	.latitude = @"latitude",
	.longitude = @"longitude",
	.magnitude = @"magnitude",
	.region = @"region",
	.timeDate = @"timeDate",
};

@implementation EarthquakeID
@end

@implementation _Earthquake

+ (id)insertInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription insertNewObjectForEntityForName:@"Earthquake" inManagedObjectContext:moc_];
}

+ (NSString*)entityName {
	return @"Earthquake";
}

+ (NSEntityDescription*)entityInManagedObjectContext:(NSManagedObjectContext*)moc_ {
	NSParameterAssert(moc_);
	return [NSEntityDescription entityForName:@"Earthquake" inManagedObjectContext:moc_];
}

- (EarthquakeID*)objectID {
	return (EarthquakeID*)[super objectID];
}

+ (NSSet*)keyPathsForValuesAffectingValueForKey:(NSString*)key {
	NSSet *keyPaths = [super keyPathsForValuesAffectingValueForKey:key];

	if ([key isEqualToString:@"depthValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"depth"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"latitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"latitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"longitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"longitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}
	if ([key isEqualToString:@"magnitudeValue"]) {
		NSSet *affectingKey = [NSSet setWithObject:@"magnitude"];
		keyPaths = [keyPaths setByAddingObjectsFromSet:affectingKey];
		return keyPaths;
	}

	return keyPaths;
}

@dynamic depth;

- (float)depthValue {
	NSNumber *result = [self depth];
	return [result floatValue];
}

- (void)setDepthValue:(float)value_ {
	[self setDepth:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveDepthValue {
	NSNumber *result = [self primitiveDepth];
	return [result floatValue];
}

- (void)setPrimitiveDepthValue:(float)value_ {
	[self setPrimitiveDepth:[NSNumber numberWithFloat:value_]];
}

@dynamic eqId;

@dynamic latitude;

- (double)latitudeValue {
	NSNumber *result = [self latitude];
	return [result doubleValue];
}

- (void)setLatitudeValue:(double)value_ {
	[self setLatitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLatitudeValue {
	NSNumber *result = [self primitiveLatitude];
	return [result doubleValue];
}

- (void)setPrimitiveLatitudeValue:(double)value_ {
	[self setPrimitiveLatitude:[NSNumber numberWithDouble:value_]];
}

@dynamic longitude;

- (double)longitudeValue {
	NSNumber *result = [self longitude];
	return [result doubleValue];
}

- (void)setLongitudeValue:(double)value_ {
	[self setLongitude:[NSNumber numberWithDouble:value_]];
}

- (double)primitiveLongitudeValue {
	NSNumber *result = [self primitiveLongitude];
	return [result doubleValue];
}

- (void)setPrimitiveLongitudeValue:(double)value_ {
	[self setPrimitiveLongitude:[NSNumber numberWithDouble:value_]];
}

@dynamic magnitude;

- (float)magnitudeValue {
	NSNumber *result = [self magnitude];
	return [result floatValue];
}

- (void)setMagnitudeValue:(float)value_ {
	[self setMagnitude:[NSNumber numberWithFloat:value_]];
}

- (float)primitiveMagnitudeValue {
	NSNumber *result = [self primitiveMagnitude];
	return [result floatValue];
}

- (void)setPrimitiveMagnitudeValue:(float)value_ {
	[self setPrimitiveMagnitude:[NSNumber numberWithFloat:value_]];
}

@dynamic region;

@dynamic timeDate;

@end

