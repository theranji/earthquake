//
//  EarthquakesDataManager.h
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface EarthquakesDataManager : NSObject


//Singleton for datamanager
+(EarthquakesDataManager*)sharedInstance;


+(RKManagedObjectStore*)getmanagedObjStore;

+(void)setupDataPersistance;

+(void)configObjectMngrs;

//get values from web service
+(void)getInitialEarthQuakes;
@end
