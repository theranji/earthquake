//
//  EarthqFilter.m
//  Earth
//
//  Created by Theranjali Nilaweera on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import "EarthqFilter.h"
#import "EarthquakesDataManager.h"

static EarthqFilter *earthqFilter = nil;

@interface EarthqFilter ()
@property (nonatomic , retain) NSDateFormatter *dateFormatter;
@end

@implementation EarthqFilter



#pragma mark - filtering the earthquakes based on region
-(NSArray*)filterWithText:(NSString*)searchTxt{
    
    //If empty return full list of incidents
    if (searchTxt.length==0) {
        return self.equakesList;
    }
    
    
    NSString *searchString= [searchTxt stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    NSArray *tokens=[searchString componentsSeparatedByString:@" "];
    NSMutableArray *regions=[[NSMutableArray alloc] init];
    
    int k=0;
    if (tokens.count>0) {
        for (NSString *tempString in tokens) {
            [regions addObject:[NSPredicate predicateWithFormat:@" SELF.region CONTAINS[c] %@ ",tempString]];
            k++;
        }
    }
    NSPredicate *totalPredicate=[NSCompoundPredicate andPredicateWithSubpredicates:regions];
    NSLog(@"totalPredicate %@",totalPredicate.description);
    
    return  [self.equakesList filteredArrayUsingPredicate:totalPredicate];
}






-(void)getInitialFetchResults
{
    
    NSFetchRequest *fetchRequest=[self setUpGenericFilter];
    
    NSFetchedResultsController *aFetchedResultsController = [[NSFetchedResultsController alloc]
                                                             initWithFetchRequest:fetchRequest managedObjectContext:[EarthquakesDataManager getmanagedObjStore].mainQueueManagedObjectContext sectionNameKeyPath:nil cacheName:@"Earthquake"];
    
    aFetchedResultsController.delegate = self;
    self.fetchedResultsController = aFetchedResultsController;
    
    NSError *error = nil;
    if (![self.fetchedResultsController performFetch:&error]) {
        NSAssert(error!=nil, @"Failed fetch initial: %@", error);
        self.equakesList=nil;
    }
    
    self.equakesList=self.fetchedResultsController.fetchedObjects;

}



#pragma mark - setup
+(EarthqFilter*)sharedInstance{
    if (earthqFilter!=nil) {
        return earthqFilter;
    }
    earthqFilter = [[EarthqFilter alloc] init];
    earthqFilter.dateFormatter.dateFormat = @"yyyy-MM-dd";
    [earthqFilter.dateFormatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [earthqFilter.dateFormatter setTimeZone:[NSTimeZone timeZoneWithAbbreviation:@"UTC"]];
    return earthqFilter;
}

-(NSFetchRequest*)setUpGenericFilter{
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *entity = [NSEntityDescription entityForName:@"Earthquake" inManagedObjectContext:[EarthquakesDataManager getmanagedObjStore].mainQueueManagedObjectContext];
    
    [fetchRequest setEntity:entity];
    [fetchRequest setFetchBatchSize:20];
    
    //latest first ordering
    NSSortDescriptor *sortDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeDate" ascending:NO];
    NSArray *sortDescriptors = @[sortDescriptor];
    
    [fetchRequest setSortDescriptors:sortDescriptors];
    return fetchRequest;
}

@end
