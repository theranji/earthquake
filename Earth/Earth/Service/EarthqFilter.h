//
//  EarthqFilter.h
//  Earth
//
//  Created by Theranjali Nilaweera on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface EarthqFilter : NSObject <NSFetchedResultsControllerDelegate>
@property (strong, nonatomic) NSFetchedResultsController *fetchedResultsController;
@property (strong, nonatomic) NSManagedObjectContext *managedObjectContext;

@property (nonatomic, retain) NSArray *equakesList;

-(void)getInitialFetchResults;
-(NSArray*)filterWithText:(NSString*)searchTxt;


+(EarthqFilter*)sharedInstance;
@end
