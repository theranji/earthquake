//
//  EarthquakesDataManager.m
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import "EarthquakesDataManager.h"
#import "Constants.h"

static EarthquakesDataManager *earthquakesDataMngr = nil;


@interface EarthquakesDataManager()
@property (nonatomic,retain)   RKManagedObjectStore *managedObjectStore;
@property (strong, nonatomic) RKObjectManager *objectMngr;
@end


@implementation EarthquakesDataManager

#pragma mark - set up data manager
+(EarthquakesDataManager*)sharedInstance{
    if (earthquakesDataMngr==nil) {
        earthquakesDataMngr = [[EarthquakesDataManager alloc] init];
    }
    return earthquakesDataMngr;
}


+(void)setupDataPersistance{
    
    NSError *error = nil;
    NSURL *modelURL = [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"EarthQuakeDataModel" ofType:@"momd"]];
    
    
    // NOTE: Due to an iOS 5 bug, the managed object model returned is immutable.
    NSManagedObjectModel *managedObjectModel = [[[NSManagedObjectModel alloc] initWithContentsOfURL:modelURL] mutableCopy];
    earthquakesDataMngr.managedObjectStore = [[RKManagedObjectStore alloc] initWithManagedObjectModel:managedObjectModel];
    
    // Initialize the Core Data stack
    [earthquakesDataMngr.managedObjectStore createPersistentStoreCoordinator];
    
    NSString *storePath = [RKApplicationDataDirectory() stringByAppendingPathComponent:@"Earthquakes.sqlite"];
    
    BOOL success = RKEnsureDirectoryExistsAtPath(RKApplicationDataDirectory(), &error);
    if (! success) {
        RKLogError(@"Failed to create Application Data Directory at path '%@': %@", RKApplicationDataDirectory(), error);
    }
    
    
    NSPersistentStore *persistentStore = [earthquakesDataMngr.managedObjectStore addSQLitePersistentStoreAtPath:storePath fromSeedDatabaseAtPath:nil  withConfiguration:nil options:@{NSMigratePersistentStoresAutomaticallyOption:@YES, NSInferMappingModelAutomaticallyOption:@YES} error:&error];
    
    
    NSAssert(persistentStore, @"Failed to add persistent store with error: %@", error);
    
    
    // Create the managed object contexts
    [earthquakesDataMngr.managedObjectStore createManagedObjectContexts];
    
    // Configure a managed object cache to ensure we do not create duplicate objects
    earthquakesDataMngr.managedObjectStore.managedObjectCache = [[RKInMemoryManagedObjectCache alloc] initWithManagedObjectContext:earthquakesDataMngr.managedObjectStore.persistentStoreManagedObjectContext];
    
    

    
}



+(RKManagedObjectStore*)getmanagedObjStore{
    return earthquakesDataMngr.managedObjectStore;
}

+(void)configObjectMngrs{
    
    earthquakesDataMngr.objectMngr = [RKObjectManager managerWithBaseURL:[NSURL URLWithString:@"http://www.seismi.org"]];
    earthquakesDataMngr.objectMngr.managedObjectStore=earthquakesDataMngr.managedObjectStore;
    
    RKEntityMapping *eqEntityMapping = [RKEntityMapping mappingForEntityForName:@"Earthquake" inManagedObjectStore:earthquakesDataMngr.managedObjectStore];
    [eqEntityMapping  addAttributeMappingsFromDictionary:@{ @"lon":     @"longitude",
                                                            @"lat":     @"latitude",
                                                            @"timedate":     @"timeDate",
                                                            @"depth":@"depth",
                                                            @"magnitude":@"magnitude",
                                                            @"region":@"region",
                                                            @"eqid": @"eqId"}];
    
    [eqEntityMapping setIdentificationAttributes:@[@"eqId"]];
    RKResponseDescriptor *eqResponseDescriptor = [RKResponseDescriptor responseDescriptorWithMapping:eqEntityMapping
                                                                                              method:RKRequestMethodGET
                                                                                         pathPattern:nil
                                                                                             keyPath:@"earthquakes"
                                                                                         statusCodes:RKStatusCodeIndexSetForClass(RKStatusCodeClassSuccessful)];
    
    
    [earthquakesDataMngr.objectMngr addResponseDescriptor:eqResponseDescriptor];
    
}

#pragma mark - web service calling
+(void)getInitialEarthQuakes{
    
    [earthquakesDataMngr.objectMngr  getObjectsAtPath:@"/api/eqs/" parameters:nil success:^(RKObjectRequestOperation *operation, RKMappingResult *mappingResult) {
        NSError *error=nil;
        [[earthquakesDataMngr.managedObjectStore mainQueueManagedObjectContext]  saveToPersistentStore:&error];
        NSAssert(error==nil, @"Failed to parse objects: %@", error);
        
        [[NSNotificationCenter defaultCenter] postNotificationName:earthquakeNotification object:self];
        NSLog(@"sending notfication");
        
    } failure:^(RKObjectRequestOperation *operation, NSError *error) {
        NSLog(@"Failed in retrieving list");
    }];
}


@end
