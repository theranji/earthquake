//
//  MapViewCtrl.h
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MapViewCtrl : UIViewController
-(void)addAnnotations;
-(void)receivedNotification:(NSNotification *) notification ;

@end
