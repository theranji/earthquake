//
//  MapViewCtrl.m
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import "MapViewCtrl.h"
#import <MapKit/MapKit.h>
#import "EarthqFilter.h"
#import "Earthquake.h"
#import "Constants.h"

@interface MapViewCtrl () <MKMapViewDelegate>
@property (nonatomic,strong)IBOutlet MKMapView *mapView;
@property (nonatomic,retain) EarthqFilter *earthqFilter;
@property (nonatomic,retain) NSDateFormatter *dateFormatter;
@end

@implementation MapViewCtrl

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if( NSFoundationVersionNumber >NSFoundationVersionNumber_iOS_6_1){
        [self setExtendedLayoutIncludesOpaqueBars:NO];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    [self setupNotification];
    [self.mapView setDelegate:self];
    
    //TODO move these two methods out to common class and inherit from that class
    [self setupInitialDisplay];
    [self setupDateFormatter];
    

    
    [self addAnnotations];
}

-(void)setupNotification{
    [[NSNotificationCenter defaultCenter]
     addObserver:self
     selector:@selector(receivedNotification:)
     name:earthquakeNotification
     object:nil];
    
}

- (void)receivedNotification:(NSNotification *) notification {
    [self.earthqFilter getInitialFetchResults];
    [self addAnnotations];
}

-(void)setupInitialDisplay{
    self.earthqFilter = [EarthqFilter sharedInstance];
    [self.earthqFilter getInitialFetchResults];
}


-(void)setupDateFormatter{
    self.dateFormatter = [NSDateFormatter new];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)addAnnotations{
    NSMutableArray *annotationArray=[[NSMutableArray alloc] init];
    for (Earthquake *incident in self.earthqFilter.equakesList) {
        MKPointAnnotation *point = [[MKPointAnnotation alloc] init];
        CLLocationCoordinate2D dLocation=CLLocationCoordinate2DMake(incident.latitude.doubleValue ,incident.longitude.doubleValue);
        
        point.coordinate = dLocation;
        point.title = [incident.region capitalizedString];
        point.subtitle = [NSString stringWithFormat:@"%@ Magnitude:%@ Depth:%@",[self.dateFormatter stringFromDate:incident.timeDate],incident.magnitude,incident.depth];
        [annotationArray addObject:point];
        
    }
    [self.mapView addAnnotations:annotationArray];

}


@end
