//
//  TableViewCtrl.h
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import <UIKit/UIKit.h>
@class EarthqFilter;

@interface TableViewCtrl : UIViewController

@property (nonatomic, retain) EarthqFilter *earthqFilter;

@end
