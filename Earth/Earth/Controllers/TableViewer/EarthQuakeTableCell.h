//
//  EarthQuakeTableCell.h
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EarthQuakeTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *regionTxt;

@property (strong, nonatomic) IBOutlet UILabel *magnitudeTxt;
@property (strong, nonatomic) IBOutlet UILabel *dateTxt;
@end
