//
//  TableViewCtrl.m
//  Earth
//
//  Created by Theranjali on 18/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import "TableViewCtrl.h"
#import "EarthqFilter.h"
#import "EarthQuakeTableCell.h"
#import "Earthquake.h"

#define earthqCellId @"EarthquakeCellId"

@interface TableViewCtrl ()  <UISearchBarDelegate, UISearchDisplayDelegate, UITableViewDataSource, UITableViewDelegate>
@property (retain,nonatomic) NSDateFormatter *dateFormatter;
@property (retain,nonatomic) NSArray *filteredEarthquakes;
@property (retain,nonatomic) IBOutlet UITableView *tableView;
@property (retain,nonatomic) IBOutlet UISearchBar *searchBar;

@end

@implementation TableViewCtrl

#pragma mark - setup
- (void)viewDidLoad {
    [super viewDidLoad];
    if( NSFoundationVersionNumber >NSFoundationVersionNumber_iOS_6_1){
        [self setExtendedLayoutIncludesOpaqueBars:NO];
        self.edgesForExtendedLayout=UIRectEdgeNone;
    }
    
    
    [self setupTable];
    [self.searchBar setDelegate:self];
    //TODO move these two methods out to common class and inherit from that class
    [self setupDateFormatter];
    [self setupInitialDisplay];
}

-(void)setupTable{
    [self.tableView setDelegate:self];
    [self.tableView setDataSource:self];
    [self.tableView registerNib:[UINib nibWithNibName:@"EarthQuakeTableCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:earthqCellId] ;
}


-(void)setupInitialDisplay{
    self.earthqFilter = [EarthqFilter sharedInstance];
    [self.earthqFilter getInitialFetchResults];
}

-(void)setupDateFormatter{
    self.dateFormatter = [NSDateFormatter new];
    self.dateFormatter.dateFormat = @"yyyy-MM-dd HH:mm";
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    if (self.searchBar.text.length>0){
        return self.filteredEarthquakes.count ;
    }
    return self.earthqFilter.equakesList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
   
    EarthQuakeTableCell *cell = [tableView dequeueReusableCellWithIdentifier:earthqCellId forIndexPath:indexPath];
   
    Earthquake *earthqData ;
    //Check if a search
    if (self.searchBar.text.length>0) {
        earthqData = [self.filteredEarthquakes objectAtIndex:indexPath.row];
    } else {
        earthqData = (Earthquake*)self.earthqFilter.equakesList[indexPath.row];
    }
    
    
    cell.regionTxt.text = [earthqData.region capitalizedString];
    cell.magnitudeTxt.text = earthqData.magnitude.stringValue;
    cell.dateTxt.text =[self.dateFormatter stringFromDate:earthqData.timeDate];
    
    return cell;
}


- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
}




#pragma mark - search bar related
- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{    
    self.filteredEarthquakes =nil;
    self.filteredEarthquakes = [self.earthqFilter filterWithText:searchText];

    [self.tableView reloadData];
    
}


@end
