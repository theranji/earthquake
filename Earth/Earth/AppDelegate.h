//
//  AppDelegate.h
//  Earth
//
//  Created by Theranjali on 17/01/2015.
//  Copyright (c) 2015 TN. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

